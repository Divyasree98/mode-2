import { Skill } from './Skill';
import { Department } from './Department';

export interface Employee{
    id:number;
    name:string;
    salary:number;
    skill:Skill[];
    //permanent:string;
    permanent:boolean;
    department:Department;
    dateofbirth:Date;
}