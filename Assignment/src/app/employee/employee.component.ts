import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
    emp:Employee={
     id:3,
     name:"John",
     salary:10000,
     permanent:true,
    // permanent:"yes",
     skill:[
      {skill_id:1,skill_name:"HTML"},
      {skill_id:2,skill_name:"CSS"},
      {skill_id:3,skill_name:"JavaScript"}
    ],
      department: {id:1,name:"Payroll"},
      dateofbirth:new Date("12/31/2000")
    
    }
    status:boolean=true;
  constructor() { }

  ngOnInit(): void {
  }

}
